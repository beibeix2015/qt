#include "user_defined_item.h"
#include <QPainterPath>
#include <QPainterPathStroker>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

MyRectItem::MyRectItem(const QRectF &rect, QGraphicsItem *parent)
    : QGraphicsRectItem(rect, parent)
{
    this->isHovered = false;
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
}

MyRectItem::MyRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent)
    : QGraphicsRectItem(x, y, w, h, parent)
{
    this->isHovered = false;
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
}

void MyRectItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen pen;
    if (this->isSelected())
    {
        pen.setColor(Qt::red);
    }
    else
    {
        pen.setColor(Qt::green);
    }
    if (this->isHovered)
    {
        pen.setWidth(4);
    }
    else
    {
        pen.setWidth(2);
    }
    painter->setPen(pen);
    painter->drawRect(this->boundingRect());
}

QPainterPath MyRectItem::shape() const
{
    QPainterPath temp;
    temp.addRect(this->boundingRect());
    QPainterPathStroker pathStroker;

    QPainterPath path = pathStroker.createStroke(temp);
    return path;
}

void MyRectItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    this->isHovered = true;
    prepareGeometryChange();
}

void MyRectItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    this->isHovered = false;
    prepareGeometryChange();
}

void MyRectItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
}

MyEllipseItem::MyEllipseItem(const QRectF &rect, QGraphicsItem *parent)
    : QGraphicsEllipseItem(rect, parent)
{
    this->isHovered = false;
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
    this->rect = rect;
    setPos(rect.center());
}

MyEllipseItem::MyEllipseItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent)
    : QGraphicsEllipseItem(x, y, w, h, parent)
{
    this->isHovered = false;
    setFlags(QGraphicsItem::ItemIsSelectable|QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
    this->rect.setX(x);
    this->rect.setY(y);
    this->rect.setWidth(w);
    this->rect.setHeight(h);
    setPos(rect.center());
}

QRectF MyEllipseItem::boundingRect() const
{
    return QRectF(-2.5,-2.5,2.5,2.5);
}

void MyEllipseItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QPen pen;
    if (this->isSelected())
    {
        pen.setColor(Qt::red);
    }
    else
    {
        pen.setColor(Qt::green);
    }
    if (this->isHovered)
    {
        pen.setWidth(4);
    }
    else
    {
        pen.setWidth(2);
    }
    painter->setPen(pen);
//    painter->drawEllipse(QRectF(0,0,5,5));
    painter->drawRect(this->rect);
}

QPointF MyEllipseItem::GetCenter() const {
    return pos();
}

QPainterPath MyEllipseItem::shape() const {
    QPainterPath temp;
    temp.addEllipse(this->boundingRect());
    QPainterPathStroker pathStroker;

    QPainterPath path = pathStroker.createStroke(temp);
    return path;
}

void MyEllipseItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    this->isHovered = true;
    prepareGeometryChange();
}

void MyEllipseItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    this->isHovered = false;
    prepareGeometryChange();
}

void MyEllipseItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    Q_UNUSED(event);
    m_last_pos = event->pos();
    QGraphicsEllipseItem::mousePressEvent(event);
}

void MyEllipseItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QPointF delta = event->pos() - m_last_pos;
    moveBy(delta.x(), delta.y());
//    setPos(mapToParent(event->pos()));

    prepareGeometryChange();
    emit SigMoved();
    m_last_pos = event->pos();
    QGraphicsEllipseItem::mouseMoveEvent(event);
}
