#include "mainwindow.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include "user_defined_item.h"
#include "user_defined_scene.h"
#include "graphics_rect_item.h"
#include <QGraphicsPolygonItem>
#include "graphics_polygon_item.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

//    QGraphicsScene scene;
    UserDefinedScene scene;
    scene.addItem(new GraphicsPolygonItem());
    QGraphicsView view(&scene);
    view.setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    view.centerOn(0,0);
    view.show();

//    MainWindow w;
//    w.show();

    return a.exec();
}
