#ifndef FUNCTION_TAB_WIDGET_H
#define FUNCTION_TAB_WIDGET_H

#include <QTabWidget>
#include <QLabel>

class FunctionTabWidget : public QTabWidget
{
    Q_OBJECT
public:
    FunctionTabWidget(QWidget *parent);
    ~FunctionTabWidget();
    int Initialize();
    int addTab(QWidget *widget, const QString &name);
protected:
    bool eventFilter(QObject * target, QEvent *event);
    void paintEvent(QPaintEvent * event) override;
    void resizeEvent(QResizeEvent *event);
};

class TabLabel : public QLabel
{
public:
    TabLabel(QWidget *parent);
protected:
    void paintEvent(QPaintEvent *event);
private:
    FunctionTabWidget *m_parent;
};

#endif // FUNCTION_TAB_WIDGET_H
