#ifndef GRAPHICS_RECT_ITEM_H
#define GRAPHICS_RECT_ITEM_H
#include <QGraphicsRectItem>
#include "subitem_manager.h"

class GraphicsRectItem : public QGraphicsRectItem
{
public:
    GraphicsRectItem(const QRectF &rect, QGraphicsItem *parent = 0);
    GraphicsRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent = 0);



private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;

//    QPainterPath shape() const Q_DECL_OVERRIDE;

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
//    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private:
    bool isHovered;
//    SubItemManager m_subitems_manager;
};

#endif // GRAPHICS_RECT_ITEM_H
