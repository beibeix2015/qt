#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "function_tab_widget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    m_function_zone = new FunctionTabWidget(this);
    m_function_zone->resize(230, 300);
    m_function_zone->setFixedWidth(230);
    //m_function_zone->move(100, 100);

    QPalette pal(m_function_zone->palette());

    //���ñ�����ɫ
    pal.setColor(QPalette::Background, Qt::black);
    m_function_zone->setAutoFillBackground(true);
    m_function_zone->setPalette(pal);
//    m_function_zone->show();

    if (nullptr != m_function_zone) {
        m_function_zone->Initialize();
        QWidget* first_tab = new QWidget();
        m_function_zone->addTab(first_tab, tr("tab_one"));
        first_tab->resize(m_function_zone->width(), m_function_zone->height());

        QPalette pal(first_tab->palette());

        //���ñ�����ɫ
        pal.setColor(QPalette::Background, Qt::black);
        first_tab->setAutoFillBackground(true);
        first_tab->setPalette(pal);
        first_tab->setFixedHeight(m_function_zone->height());

        QWidget* sec_tab = new QWidget();

        pal = sec_tab->palette();

        //���ñ�����ɫ
        pal.setColor(QPalette::Background, Qt::blue);
        sec_tab->setAutoFillBackground(true);
        sec_tab->setPalette(pal);

        m_function_zone->addTab(sec_tab, tr("tab_two"));
        sec_tab->resize(m_function_zone->width(), m_function_zone->height());
        sec_tab->setFixedHeight(m_function_zone->height());
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
