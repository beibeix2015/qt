/**
* @projectName   QtWidget
* @brief         图形项管理器，用于管理
* @author        Beibeix
* @date          2020-02-28
*/
#ifndef SUBITEM_MANAGER_H
#define SUBITEM_MANAGER_H

#include <QObject>
#include <QVector>
#include <QList>
#include <QRectF>


class GraphicsRectItem;
class MyEllipseItem;
class QGraphicsLineItem;
class GraphicsCircleItem;
class GraphicsLineItem;
class GraphicsPolygonItem;
enum class SelectedEdgeType;


class SubItemManager : public QObject
{
    Q_OBJECT
public:
    explicit SubItemManager(GraphicsPolygonItem *item);
    QRectF GetOuterRectF() const { return m_outer_rect; }
    void UpdateBoundingRect();

private:
    /**
      * 创建点对象并建立相关的信号-槽连接，并将对象添加到容器中管理
      * 参数：index表示点的序号
      * 参数：center表示该图形项在父坐标系下的坐标
    */
    GraphicsCircleItem *CreatePointItem(unsigned int index, QPointF center);
    /**
      * 创建线段对象并建立相关的信号-槽连接，并将对象添加到容器中管理
      * 参数：index表示点的序号
    */
    GraphicsLineItem *CreateLineItem(unsigned int index);

    /**
      * 设置点与点之间的连线
      *
    */
    void SetConnectionLines();
    /**
     * 更新点群的外接矩形
    */
    void UpdateOuterRect();

private slots:
    /**
     * 点移动的响应槽函数
     **/
    void SltPointMoved();
    /**
     * 边线移动响应槽函数
     **/
    void SltEdgeMoved();
    /**
     * 移除端点图形项响应槽函数
     * 参数：端点对应的图形项
     **/
    void SltPointRemove(GraphicsCircleItem *item);
    /**
     * 连线上增加端点响应槽函数
     * 参数1：线图形项
     * 参数2：点的坐标
     **/
    void SltAddPoint(GraphicsLineItem *item, const QPointF &pt);

signals:
    /**
     * @brief SigAddPointIndexUpdate 新增点更新索引的信号
     * 参数：新增点的索引，用于点群更新当前的索引
     */
    void SigAddPointIndexUpdate(unsigned int);
    /**
      * 移除点更新索引的信号
      * 参数：移除点的索引，用于点群更新当前的索引
    */
    void SigRemovePointIndex(unsigned int);
    /**
     * 更新点群最小外接矩形的信号，点接受该信号用以更新移动比率
     * 参数：当前的最小外接矩形
     */
    void SigUpdateBoundingRect(const QRectF &rt);

//    void SigSelectedEdgeMove(SelectedEdgeType,qreal);

private:
    GraphicsPolygonItem *m_parant_item;
    QList<GraphicsCircleItem*> m_sub_circle_items;
    QList<GraphicsLineItem*> m_sub_line_items;
    QRectF m_outer_rect;//外接矩形

    static int m_init_points_count;

    typedef QList<GraphicsCircleItem*>::size_type SizeType;
};

#endif // SUBITEM_MANAGER_H
