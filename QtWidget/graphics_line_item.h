#ifndef GRAPHICS_LINE_ITEM_H
#define GRAPHICS_LINE_ITEM_H

#include <QGraphicsLineItem>
class SubItemManager;

class GraphicsLineItem : public QObject, public QGraphicsLineItem
{
    Q_OBJECT
public:
    explicit GraphicsLineItem(const unsigned int& index, QGraphicsItem *parent = Q_NULLPTR);

    unsigned int GetIndex() const { return m_index; }
    void SetIndex(const unsigned int & index) { m_index = index; }

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private slots:
    void SltAddPointIndex(unsigned int index);
    void SltRemovePointIndex(unsigned int index);

signals:
    void SigAddPoint(GraphicsLineItem*, const QPointF&);

private:
    bool m_is_hovered;
    unsigned int m_index;
    QPointF m_cursor_point;
    const int m_pen_width;
    const int m_hovered_pen_width;
};

#endif // GRAPHICS_LINE_ITEM_H
