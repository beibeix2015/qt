#include "graphics_rect_item.h"
#include <QPainterPath>
#include <QPainterPathStroker>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QCursor>

GraphicsRectItem::GraphicsRectItem(const QRectF &rect, QGraphicsItem *parent)
    : QGraphicsRectItem(rect, parent)/*,
      m_subitems_manager(this)*/
{
    this->isHovered = false;
    setFlags(QGraphicsItem::ItemIsFocusable|QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
}

GraphicsRectItem::GraphicsRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent)
    : QGraphicsRectItem(x, y, w, h, parent)/*,
      m_subitems_manager(this)*/
{
    this->isHovered = false;
    setFlags(QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
//    setPos(-w/2, -h/2);
}

void GraphicsRectItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen pen;    
    pen.setWidth(2);
    pen.setColor(Qt::red);
    painter->setPen(pen);
    setRect(boundingRect());//设置父类中的rect数据成员
//    painter->drawRect(rect());
//    painter->drawRect(boundingRect());
    pen.setColor(QColor(0,255,0,125));
//    if (this->isHovered)
//    {
//        pen.setWidth(4);
//    }
//    else
//    {
//        pen.setWidth(2);
//    }

    painter->setPen(pen);
//    painter->drawRect(m_subitems_manager.GetOuterRectF());
}

QRectF GraphicsRectItem::boundingRect() const {
    QRectF rt /*= m_subitems_manager.GetOuterRectF()*/;
    QPointF topleft = rt.topLeft(), bottomright = rt.bottomRight();
    //外扩5个单位
    QRectF bounding_rect(topleft.x()-10, topleft.y()-10, rt.width()+20, rt.height()+20);
    return bounding_rect;
}

//QPainterPath GraphicsRectItem::shape() const
//{
//    QPainterPath temp;
//    temp.addRect(m_subitems_manager.GetOuterRectF());
//    QPainterPathStroker pathStroker;

//    QPainterPath path = pathStroker.createStroke(temp);
//    return path;
//}

//void GraphicsRectItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
//    Q_UNUSED(event);
//    this->isHovered = true;
//    prepareGeometryChange();
//}

//void GraphicsRectItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
//    Q_UNUSED(event);
//    this->isHovered = false;
//    prepareGeometryChange();
//}

void GraphicsRectItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
Q_UNUSED(event);
}

void GraphicsRectItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    setCursor(QCursor(Qt::ArrowCursor));
}

void GraphicsRectItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event) {
//    qDebug()<<"mouseMoveEvent";
    QPainterPath hori_path, vert_path;
//    hori_path.moveTo(m_subitems_manager.GetOuterRectF().topLeft());
//    hori_path.lineTo(m_subitems_manager.GetOuterRectF().topRight());

//    hori_path.moveTo(m_subitems_manager.GetOuterRectF().bottomLeft());
//    hori_path.lineTo(m_subitems_manager.GetOuterRectF().bottomRight());

//    vert_path.moveTo(m_subitems_manager.GetOuterRectF().topLeft());
//    vert_path.lineTo(m_subitems_manager.GetOuterRectF().bottomLeft());

//    vert_path.moveTo(m_subitems_manager.GetOuterRectF().topRight());
//    vert_path.lineTo(m_subitems_manager.GetOuterRectF().bottomRight());

    QPainterPathStroker pathStroker;
    pathStroker.setWidth(10);
    QPainterPath hori_stroker_path = pathStroker.createStroke(hori_path);
    QPainterPath vert_stroker_path = pathStroker.createStroke(vert_path);

    if (hori_stroker_path.contains(event->pos())) {
        setCursor(QCursor(Qt::SizeVerCursor));
    } else  if (vert_stroker_path.contains(event->pos())) {
        setCursor(QCursor(Qt::SizeHorCursor));
    } else {
        setCursor(QCursor(Qt::ArrowCursor));
    }

//    setCursor(QCursor(Qt::UpArrowCursor));
}

void GraphicsRectItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
//    qDebug()<<"mouseMoveEvent";
    QPointF delta = event->pos() - event->lastPos();
    moveBy(delta.x(), delta.y());
//    setPos(event->scenePos());

    //根据Cursor类型，实现边框的
}
