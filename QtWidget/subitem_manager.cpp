#include "subitem_manager.h"
#include "user_defined_item.h"
#include "graphics_rect_item.h"
#include <QGraphicsLineItem>
#include <QDebug>
#include "graphics_circle_item.h"
#include "graphics_line_item.h"
#include "graphics_polygon_item.h"

int SubItemManager::m_init_points_count = 3;
SubItemManager::SubItemManager(GraphicsPolygonItem *item) :
    m_parant_item(item)
{
    GraphicsCircleItem *circle_item = nullptr;
     circle_item = CreatePointItem(0, QPointF(10, 10));
    m_sub_circle_items.push_back(circle_item);
    circle_item = CreatePointItem(1, QPointF(50, 50));
    m_sub_circle_items.push_back(circle_item);
    circle_item = CreatePointItem(2, QPointF(50, 150));
    m_sub_circle_items.push_back(circle_item);

    GraphicsLineItem *line_item = nullptr;
    for (QList<GraphicsCircleItem*>::size_type i = 0; i < m_sub_circle_items.size(); ++i) {
        unsigned int index = static_cast<unsigned int>(i);
        line_item = CreateLineItem(index);
        m_sub_line_items.push_back(line_item);
    }

    SetConnectionLines();
    UpdateOuterRect();

    //更新点与边界框关系
    emit SigUpdateBoundingRect(m_outer_rect);
}

void SubItemManager::UpdateBoundingRect() {
    emit SigUpdateBoundingRect(m_outer_rect);
}

GraphicsCircleItem * SubItemManager::CreatePointItem(unsigned int index, QPointF center) {
    //创建点对象
    GraphicsCircleItem *circle_item = nullptr;
    circle_item = new GraphicsCircleItem(index, center, m_parant_item);
    //建立信号-槽
    connect(circle_item, SIGNAL(SigRemove(GraphicsCircleItem*)), this, SLOT(SltPointRemove(GraphicsCircleItem*)));
    connect(circle_item, SIGNAL(SigMoved()), this, SLOT(SltPointMoved()));
    connect(circle_item, SIGNAL(SigEdgeMoved()), this, SLOT(SltEdgeMoved()));
    connect(this, SIGNAL(SigAddPointIndexUpdate(uint)), circle_item, SLOT(SltAddPointIndex(uint)));
    connect(this, SIGNAL(SigRemovePointIndex(uint)), circle_item, SLOT(SltRemovePointIndex(uint)));
    connect(this, SIGNAL(SigUpdateBoundingRect(QRectF)), circle_item, SLOT(SltBoundaryRectUpdate(QRectF)));
    connect(m_parant_item, SIGNAL(SigEdgeMove(SelectedEdgeType,qreal)), circle_item, SLOT(SltSelectedEdgeMove(SelectedEdgeType,qreal)));
    return circle_item;
}

GraphicsLineItem * SubItemManager::CreateLineItem(unsigned int index) {
    //创建线对象
    GraphicsLineItem *line_item = nullptr;
    line_item = new GraphicsLineItem(index, m_parant_item);
    //建立信号-槽
    connect(line_item, SIGNAL(SigAddPoint(GraphicsLineItem*,QPointF)), this, SLOT(SltAddPoint(GraphicsLineItem*,QPointF)));
    connect(this, SIGNAL(SigAddPointIndexUpdate(uint)), line_item, SLOT(SltAddPointIndex(uint)));
    return line_item;
}

void SubItemManager::SetConnectionLines() {
    //画连线
    QPointF pt_s, pt_e;
    for(QList<QGraphicsLineItem*>::size_type i = 0; i < m_sub_line_items.size() - 1; ++i) {
        pt_s = m_sub_circle_items[i]->GetCircleCenter();
        pt_e = m_sub_circle_items[i+1]->GetCircleCenter();
        m_sub_line_items[i]->setLine(QLineF(pt_s, pt_e));
    }
    pt_s = m_sub_circle_items[0]->GetCircleCenter();
    m_sub_line_items[m_sub_line_items.size()-1]->setLine(QLineF(pt_s, pt_e));//最后一根连线
}

void SubItemManager::UpdateOuterRect() {
    QPointF topleft = m_sub_circle_items[0]->GetCircleCenter(), bottomright = m_sub_circle_items[0]->GetCircleCenter();
    for(SizeType i = 1; i < m_sub_circle_items.size(); ++i) {
        QPointF pt = m_sub_circle_items[i]->GetCircleCenter();
        if (topleft.x()>pt.x()) {
            topleft.setX(pt.x());
        }
        if(topleft.y()>pt.y()) {
            topleft.setY(pt.y());
        }

        if (bottomright.x()<pt.x()) {
            bottomright.setX(pt.x());
        }
        if (bottomright.y()<pt.y()) {
            bottomright.setY(pt.y());
        }
    }

    m_outer_rect.setTopLeft(topleft);
    m_outer_rect.setBottomRight(bottomright);
}

void SubItemManager::SltPointMoved() {
    SetConnectionLines();
    UpdateOuterRect();
    //更新点与边界框关系
    emit SigUpdateBoundingRect(m_outer_rect);
}

void SubItemManager::SltEdgeMoved() {
    SetConnectionLines();
    UpdateOuterRect();
}

void SubItemManager::SltPointRemove(GraphicsCircleItem *item) {
    if (m_sub_circle_items.size() > m_init_points_count) {//最小个数为m_init_points_count
        const unsigned int index = item->GetIndex();
        //广播当前移除的点的索引
        emit SigRemovePointIndex(index);
        //移除端点并释放该图形项
        m_sub_circle_items.removeAt(index);
        QGraphicsScene *scene = m_parant_item->scene();
        scene->removeItem(item);
        delete item;

        GraphicsLineItem *line_item = m_sub_line_items.at(index);
        m_sub_line_items.removeAt(index);
        scene->removeItem(line_item);
        delete line_item;

        SetConnectionLines();
        UpdateOuterRect();

        //更新点与边界框关系
        emit SigUpdateBoundingRect(m_outer_rect);
    }
}

void SubItemManager::SltAddPoint(GraphicsLineItem *item, const QPointF &pt) {
    const unsigned int index = item->GetIndex();
    //广播当前添加的点所在直线的索引
    emit SigAddPointIndexUpdate(index);
    //增加端点和线段
    GraphicsCircleItem *circle_item = CreatePointItem(index+1, pt);
    m_sub_circle_items.insert(index+1, circle_item);
    GraphicsLineItem *line_item = CreateLineItem(index+1);
    m_sub_line_items.insert(index+1, line_item);

    SetConnectionLines();
    UpdateOuterRect();

    //更新点与边界框关系
    emit SigUpdateBoundingRect(m_outer_rect);
}
