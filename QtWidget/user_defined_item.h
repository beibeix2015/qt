/**
* @projectName   QtWidget
* @brief         摘要
* @author        Beibeix
* @date          2020-02-28
* @filename      user_defined_item.h
*/
#ifndef USER_DEFINED_ITEM_H
#define USER_DEFINED_ITEM_H

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsPathItem>
#include <QGraphicsEllipseItem>

namespace Ui {
class Widget;
}

class MyRectItem : public QGraphicsRectItem
{
public:
    MyRectItem(const QRectF &rect, QGraphicsItem *parent = 0);
    MyRectItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent = 0);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QPainterPath shape() const;

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private:
    bool isHovered;
};

class MyEllipseItem : public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
public:
    MyEllipseItem(const QRectF &rect, QGraphicsItem *parent = 0);
    MyEllipseItem(qreal x, qreal y, qreal w, qreal h, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QPointF GetCenter() const;

    QPainterPath shape() const;

signals:
    void SigMoved();

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private:
    bool isHovered;
    QRectF rect;
    QPointF m_last_pos;
};

#endif // USER_DEFINED_ITEM_H
