#ifndef GRAPHICS_CIRCLE_ITEM_H
#define GRAPHICS_CIRCLE_ITEM_H

#include <QGraphicsEllipseItem>

class SubItemManager;
enum class SelectedEdgeType;



class GraphicsCircleItem : public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT
public:
//    explicit GraphicsCircleItem(QGraphicsItem *parent = Q_NULLPTR);
    GraphicsCircleItem(unsigned int index, QPointF center, QGraphicsItem *parent = Q_NULLPTR);
//    explicit GraphicsCircleItem(qreal x, qreal y, qreal r, QGraphicsItem *parent = Q_NULLPTR);

    QPointF GetCircleCenter() const { return m_center; }

    unsigned int GetIndex() const { return m_index; }

//    void SetIndex(const unsigned int & index) { m_index = index; }
//    PointType GetPointType() const { return m_point_type; }


private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;

    bool IsBottomEdgePoint() const;
    bool IsTopEdgePoint() const;
    bool IsLeftEdgePoint() const;
    bool IsRightEdgePoint() const;

signals:
    void SigMoved();
    void SigRemove(GraphicsCircleItem*);
    void SigEdgeMoved();

private slots:
    /**
      * 点的上下文菜单中，移除动作的响应槽函数
      **/
    void SltRemoveTriggered();
    /**
      * 点的索引是否需要变更，如果索引大于新增点的索引则需要变更，否则，无需更新
      * 参数：index为新增点的索引
      **/
    void SltAddPointIndex(unsigned int index);
    /**
      * 点的索引是否需要变更，如果索引大于删除点的索引则需要变更，否则，无需更新
      * 参数：index为删除点的索引
      **/
    void SltRemovePointIndex(unsigned int index);
    /**
      * 点群的外接矩形变化，更新点的位置信息（点的类型，边界上的点、角点还是内点），以及点到四条边的距离
      **/
    void SltBoundaryRectUpdate(const QRectF &rt);
    /**
      * 点群的外接矩形边移动的响应函数
      * 参数：edge_type是边的类型（上、下、左、右）
      * 参数：distance边移动的距离，用于计算点需要移动的距离
      **/
    void SltSelectedEdgeMove(SelectedEdgeType edge_type, qreal distance);


private:
    QPointF m_center;//点的位置，点用圆来表示，亦即圆心，父坐标系下的坐标值
    const qreal m_radius;//圆的半径
    unsigned int m_index;//点的索引，用于增加、删除点，记录点的顺序

    bool m_left_mouse_pressed;

    enum PointType {
        INNER_POINT,//内点
        LEFT_EDGE_POINT,//边界点
        RIGHT_EDGE_POINT,
        TOP_EDGE_POINT,
        BOTTOM_EDGE_POINT,
        TOPLEFT_CORNER_POINT,//角点
        TOPRIGHT_CORNER_POINT,
        BOTTOMLEFT_CORNER_POINT,
        BOTTOMRIGHT_CORNER_POINT,
    };

    PointType m_point_type;//点的类型

    struct DistToBoundary {
        qreal top_ratio;
        qreal bottom_ratio;
        qreal left_ratio;
        qreal right_ratio;
    };
    DistToBoundary m_dist_to_boundary;//点到边界框的距离
    const qreal m_z_value;
};

#endif // GRAPHICS_CIRCLE_ITEM_H
