#-------------------------------------------------
#
# Project created by QtCreator 2019-03-22T11:17:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtWidget
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    function_tab_widget.cpp \
    user_defined_item.cpp \
    user_defined_scene.cpp \
    graphics_rect_item.cpp \
    subitem_manager.cpp \
    graphics_circle_item.cpp \
    graphics_line_item.cpp \
    graphics_polygon_item.cpp

HEADERS  += mainwindow.h \
    function_tab_widget.h \
    user_defined_item.h \
    user_defined_scene.h \
    graphics_rect_item.h \
    subitem_manager.h \
    graphics_circle_item.h \
    graphics_line_item.h \
    graphics_polygon_item.h \
    123.h

FORMS    += mainwindow.ui
