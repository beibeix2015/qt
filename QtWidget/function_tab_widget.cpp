#include "function_tab_widget.h"
#include <QEvent>
#include <QPainter>
#include <QVBoxLayout>
#include <QTabBar>
#include <QDebug>

FunctionTabWidget::FunctionTabWidget(QWidget *parent)
    : QTabWidget(parent) {

}

FunctionTabWidget::~FunctionTabWidget() {

}

int FunctionTabWidget::Initialize() {
    setObjectName("FunctionTab");
    tabBar()->setObjectName("FunctionTabBar");
    //installEventFilter(this);
    return 0;
}

int FunctionTabWidget::addTab(QWidget *widget, const QString &name) {
    QWidget *tab_main = new QWidget(this);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);
    TabLabel *label = new TabLabel(this);
    label->setAutoFillBackground(true);
    layout->addWidget(label);
    layout->addWidget(widget);
    tab_main->setLayout(layout);
    return QTabWidget::addTab(tab_main, name);
}

bool FunctionTabWidget::eventFilter(QObject *target, QEvent *event) {
    if(event->type() == QEvent::Resize) {

    }
    return QTabWidget::eventFilter(target, event);
}

void FunctionTabWidget::paintEvent(QPaintEvent *event)
{
    qDebug()<<this->size();
}

void FunctionTabWidget::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    int total_count = count();
    if (0 == total_count) {
        return;
    }
    int total_width = width();
    QString style = "QTabBar::tab {"
            "width:"
            + QString::number(total_width / total_count, 'f', 0)
            + "px;"
            "font: bold 12px;"
            "background-color: rgb(241, 241, 241);"
            "padding-top: 10px;"
            "}"
    + "QTabWidget::pane { border: 0px;}"
    + "QTabBar::tab:selected, "
            "QTabBar::tab:hover {"
        "color: rgb(121, 175, 255);}";
    setStyleSheet(style);
}

TabLabel::TabLabel(QWidget *parent) : QLabel(parent) {
    m_parent = (FunctionTabWidget*)parent;

    QPalette pal(palette());

    //���ñ�����ɫ
    pal.setColor(QPalette::Background, Qt::red);
    setAutoFillBackground(true);
    setPalette(pal);
}

void TabLabel::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event);
    int trigle_size = 8;
    int total_count = m_parent->count();
    int total_width = m_parent->width();
    int current_tab = m_parent->currentIndex();
    QPainter painter(this);
    //painter.setRenderHints(QPainter::Antialiasing, true);
    painter.setPen(QPen(QColor(197, 197, 197), 1));
    QPointF left, top, right;
    double tab_width = total_width / total_count;
    trigle_size = trigle_size / 1.414;
    double x = tab_width * current_tab + tab_width / 2;
    left = QPointF(x - trigle_size, 10);
    top = QPointF(x, 10 - trigle_size);
    right = QPointF(x + trigle_size, 10);
    painter.drawLine(QPointF(4, 10), left);
    painter.drawLine(left, top);
    painter.drawLine(top, right);
    painter.drawLine(right, QPointF(total_width - 4, 10));
}
