#include "graphics_circle_item.h"
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QCursor>
#include <QDebug>
#include <QMenu>
#include "subitem_manager.h"
#include "graphics_polygon_item.h"

GraphicsCircleItem::GraphicsCircleItem(unsigned int index, QPointF center, QGraphicsItem *parent) :
    QGraphicsEllipseItem(parent),
    m_center(center),
    m_radius(5),
    m_index(index),
    m_point_type(PointType::INNER_POINT),
    m_z_value(1)
{
    setFlags(QGraphicsItem::ItemIsFocusable|QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
    setPos(m_center);//父图形项坐标系下的坐标
    setRect(QRectF(QPointF(-QPointF(m_radius, m_radius)), QSizeF(2*m_radius, 2*m_radius)));//自身图形项坐标系的坐标下的矩形
    setZValue(m_z_value);
}

void GraphicsCircleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen pen;
    pen.setColor(Qt::red);
    painter->setPen(pen);
    painter->drawEllipse(boundingRect());
    painter->drawText(boundingRect().center(), QString::number(m_index));
}

void GraphicsCircleItem::SltRemoveTriggered() {
    emit SigRemove(this);
}

void GraphicsCircleItem::SltAddPointIndex(unsigned int index) {
    if (m_index > index) {
        ++m_index;
    }
}

void GraphicsCircleItem::SltRemovePointIndex(unsigned int index) {
    if (m_index > index) {
        --m_index;
    }
}

void GraphicsCircleItem::SltBoundaryRectUpdate(const QRectF &rt) {
    //保存比例信息，用于边界拖动
    m_dist_to_boundary.top_ratio = (m_center.y() - rt.top()) / rt.height();
    m_dist_to_boundary.bottom_ratio = (rt.bottom() - m_center.y()) / rt.height();
    m_dist_to_boundary.left_ratio = (m_center.x() - rt.left()) / rt.width();
    m_dist_to_boundary.right_ratio = (rt.right() - m_center.x()) / rt.width();

    qreal x = m_center.x(), y = m_center.y();
    if (rt.left() < x && rt.right() > x
            && rt.top() < y && rt.bottom() > y) {//判断是否为内点
        m_point_type = INNER_POINT;
    } else if (rt.left() == x) {//左边上的点
        if (rt.top() == y) {
            m_point_type = TOPLEFT_CORNER_POINT;
        } else if (rt.bottom() == y) {
            m_point_type = BOTTOMLEFT_CORNER_POINT;
        } else {
            m_point_type = LEFT_EDGE_POINT;
        }
    } else if (rt.right() == x) {//右边上的点
        if (rt.top() == y) {
            m_point_type = TOPRIGHT_CORNER_POINT;
        } else if (rt.bottom() == y) {
            m_point_type = BOTTOMRIGHT_CORNER_POINT;
        } else {
            m_point_type = RIGHT_EDGE_POINT;
        }
    } else if (rt.top() == y) {//上边上的点
        if (rt.right() == x) {
            m_point_type = TOPRIGHT_CORNER_POINT;
        } else if (rt.left() == x) {
            m_point_type = TOPLEFT_CORNER_POINT;
        } else {
            m_point_type = TOP_EDGE_POINT;
        }
    } else if (rt.bottom() == y) {//下边上的点
        if (rt.right() == x) {
            m_point_type = BOTTOMRIGHT_CORNER_POINT;
        } else if (rt.left() == x) {
            m_point_type = BOTTOMLEFT_CORNER_POINT;
        } else {
            m_point_type = BOTTOM_EDGE_POINT;
        }
    }
//    qDebug()<<rt<<"SltBoundaryRectUpdate: "<<m_index <<"  "<< m_center<<"  "<<m_point_type;
}

void GraphicsCircleItem::SltSelectedEdgeMove(SelectedEdgeType edge_type, qreal distance) {
    qreal move_ratio = 1.0;
    QPointF delta(0.0, 0.0);
    if(SelectedEdgeType::EDGE_UP == edge_type) {//上边移动
        if (IsBottomEdgePoint()) {//下边上的点不动
            move_ratio = 0.0;
        } else if (IsTopEdgePoint()){
            move_ratio = 1.0;
        } else {
            move_ratio = m_dist_to_boundary.bottom_ratio;
        }
        delta.setY(move_ratio*distance);
    } else if(SelectedEdgeType::EDGE_DOWN == edge_type) {
        if (IsBottomEdgePoint()) {//下边上的点不动
            move_ratio = 1.0;
        } else if (IsTopEdgePoint()){
            move_ratio = 0.0;
        } else {
            move_ratio = m_dist_to_boundary.top_ratio;
        }
        delta.setY(move_ratio*distance);
    } else if(SelectedEdgeType::EDGE_LEFT == edge_type) {
        if (IsLeftEdgePoint()) {
            move_ratio = 1.0;
        } else if (IsRightEdgePoint()) {//右边上的点不动
            move_ratio = 0.0;
        } else {
            move_ratio = m_dist_to_boundary.right_ratio;
        }
        delta.setX(move_ratio*distance);
    } else if(SelectedEdgeType::EDGE_RIGHT == edge_type) {
        if (IsLeftEdgePoint()) {
            move_ratio = 0.0;
        } else if (IsRightEdgePoint()) {//右边上的点不动
            move_ratio = 1.0;
        } else {
            move_ratio = m_dist_to_boundary.left_ratio;
        }
        delta.setX(move_ratio*distance);
    }

    moveBy(delta.x(), delta.y());
    m_center += delta;
    emit SigEdgeMoved();
}

void GraphicsCircleItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    setCursor(QCursor(Qt::SizeAllCursor));
}

void GraphicsCircleItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    setCursor(QCursor(Qt::ArrowCursor));
}

void GraphicsCircleItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (Qt::LeftButton == event->button()) {
        m_left_mouse_pressed = true;
    } else {
        m_left_mouse_pressed = false;
    }
}

void GraphicsCircleItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
//    qDebug()<<event->button();
    if (m_left_mouse_pressed) {
        setPos(mapToParent(event->pos()));
        m_center = mapToParent(event->pos());
        emit SigMoved();
    }
}

void GraphicsCircleItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    if (Qt::LeftButton == event->button()) {//鼠标左键释放响应
        setPos(mapToParent(event->pos()));
        m_center = mapToParent(event->pos());
        emit SigMoved();
    }

    m_left_mouse_pressed = false;
}

void GraphicsCircleItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {
    Q_UNUSED(event);
    QMenu mymenu;
    QAction *removeAction = mymenu.addAction("Remove");
    connect(removeAction, SIGNAL(triggered()), this, SLOT(SltRemoveTriggered())); //释放移除信号
    mymenu.exec(QCursor::pos());
}

bool GraphicsCircleItem::IsBottomEdgePoint() const {
    return BOTTOM_EDGE_POINT == m_point_type
            || BOTTOMLEFT_CORNER_POINT == m_point_type
            || BOTTOMRIGHT_CORNER_POINT == m_point_type;
}

bool GraphicsCircleItem::IsTopEdgePoint() const {
    return TOP_EDGE_POINT == m_point_type
            || TOPLEFT_CORNER_POINT == m_point_type
            || TOPRIGHT_CORNER_POINT == m_point_type;
}

bool GraphicsCircleItem::IsLeftEdgePoint() const {
    return LEFT_EDGE_POINT == m_point_type
            || BOTTOMLEFT_CORNER_POINT == m_point_type
            || TOPLEFT_CORNER_POINT == m_point_type;
}

bool GraphicsCircleItem::IsRightEdgePoint() const {
    return RIGHT_EDGE_POINT == m_point_type
            || BOTTOMRIGHT_CORNER_POINT == m_point_type
            || TOPRIGHT_CORNER_POINT == m_point_type;
}
