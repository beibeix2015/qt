/**
* @projectName   QtWidget
* @brief         多边形图形项，用于与场景交互以及提供多边形相关的接口
* @author        Beibeix
* @date          2020-02-28
*/
#ifndef GRAPHICSPOLYGONITEM_H
#define GRAPHICSPOLYGONITEM_H

#include <QGraphicsItem>
#include "subitem_manager.h"

enum class SelectedEdgeType {
    EDGE_UP,
    EDGE_DOWN,
    EDGE_LEFT,
    EDGE_RIGHT,
    NONE
};

class GraphicsPolygonItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit GraphicsPolygonItem(QGraphicsItem *parent = 0);

private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;

private:
    enum class OperationType {
        HORI_EDGE_MOVE,
        VERT_EDGE_MOVE,
        POLYGON_MOVE,
        NONE
    };

    OperationType CursorTypeToOperation(Qt::CursorShape cursor_shape) const;

    QPainterPath UpEdgePath() const;
    QPainterPath DownEdgePath() const;
    QPainterPath RightEdgePath() const;
    QPainterPath LeftEdgePath() const;

    bool OnHorizontalEdge(const QPointF &pt) const;
    bool OnVerticalEdge(const QPointF &pt) const;

signals:
    /**
     * @brief SigEdgeMove函数用于发送边移动的信号
     * 参数1：选中的边的类型，分为上、下、左、右
     * 参数2：移动的距离（带方向）
     */
    void SigEdgeMove(SelectedEdgeType, qreal);

private:
    SubItemManager m_subitems_manager;
    const qreal m_dash_rate;
    SelectedEdgeType m_selected_edge_type;
    QPainterPathStroker m_path_stroker;
    bool m_left_mouse_btn_pressed;
    const int m_corner_width;

};

#endif // GRAPHICSPOLYGONITEM_H
