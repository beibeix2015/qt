#ifndef USERDEFINEDSCENE_H
#define USERDEFINEDSCENE_H
#include <QGraphicsScene>

class UserDefinedScene : public QGraphicsScene
{
    Q_OBJECT
public:
    UserDefinedScene();


protected:
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;

private slots:
    void slotRemoveItem();

};

#endif // USERDEFINEDSCENE_H
