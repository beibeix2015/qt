#include "graphics_line_item.h"
#include <QCursor>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include "subitem_manager.h"

GraphicsLineItem::GraphicsLineItem(const unsigned int& index, QGraphicsItem *parent) :
    QGraphicsLineItem(parent),
    m_is_hovered(false),
    m_index(index),
    m_cursor_point(),
    m_pen_width(2),
    m_hovered_pen_width(3)
{
    setAcceptHoverEvents(true);
    setFlag(QGraphicsItem::ItemIsSelectable);
}

void GraphicsLineItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    setCursor(QCursor(Qt::CrossCursor));
    m_cursor_point = event->pos();
    m_is_hovered = true;
    prepareGeometryChange();
}

void GraphicsLineItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    setCursor(QCursor(Qt::ArrowCursor));
    m_is_hovered = false;
    prepareGeometryChange();
}

void GraphicsLineItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen pen;
    if (m_is_hovered) {
//        pen.setWidth(2);
//        pen.setColor(Qt::green);
//        painter->setPen(pen);
//        painter->drawEllipse(m_cursor_point, 4, 4);
        pen.setWidth(m_hovered_pen_width);
        pen.setColor(Qt::black);
        painter->setPen(pen);
        painter->drawLine(line());
    } else {
        pen.setWidth(m_pen_width);
        painter->setPen(pen);
        painter->drawLine(line());
    }
}

void GraphicsLineItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (Qt::LeftButton == event->button()) {//鼠标左键按下响应
        QPointF pt = mapToParent(event->pos());
        emit SigAddPoint(this, pt);
    }
}

void GraphicsLineItem::SltAddPointIndex(unsigned int index) {
    if (m_index > index) {
        ++m_index;
    }
}

void GraphicsLineItem::SltRemovePointIndex(unsigned int index) {
    if (m_index > index) {
        --m_index;
    }
}
