#include "graphics_polygon_item.h"

#include <QCursor>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QPen>
#include <QDebug>

GraphicsPolygonItem::GraphicsPolygonItem(QGraphicsItem *parent) :
    QGraphicsItem(parent),
    m_subitems_manager(this),
    m_dash_rate(4),
    m_selected_edge_type(SelectedEdgeType::NONE),
    m_left_mouse_btn_pressed(false),
    m_corner_width(10)
{
    setFlags(QGraphicsItem::ItemIsMovable);
    setAcceptHoverEvents(true);
    m_path_stroker.setWidth(10);
}

void GraphicsPolygonItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QPen pen;

//    painter->setPen(pen);
//    pen.setColor(QColor(255,0,0,125));

//    painter->drawPath(UpEdgePath());
//    painter->drawPath(LeftEdgePath());



    pen.setWidth(2);//TODO:随着视图的缩放而变化
    pen.setColor(Qt::red);


    QVector<qreal> dashes;
    qreal space = m_dash_rate;
    dashes << m_dash_rate << space;
    pen.setDashPattern(dashes);
    painter->setPen(pen);
    painter->drawRect(m_subitems_manager.GetOuterRectF());
}

QRectF GraphicsPolygonItem::boundingRect() const {
    QRectF rt = m_subitems_manager.GetOuterRectF();
    QPointF topleft = rt.topLeft();
    //外扩5个单位
    QRectF bounding_rect(topleft.x(), topleft.y(), rt.width(), rt.height());
    return bounding_rect;
}

void GraphicsPolygonItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
}

void GraphicsPolygonItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    Q_UNUSED(event);
    setCursor(QCursor(Qt::ArrowCursor));
}

void GraphicsPolygonItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event) {
    QPointF pt = event->pos();
    if (OnHorizontalEdge(pt)) {
        setCursor(QCursor(Qt::SizeVerCursor));
    } else  if (OnVerticalEdge(pt)) {
        setCursor(QCursor(Qt::SizeHorCursor));
    } else {
        setCursor(QCursor(Qt::SizeAllCursor));
    }
}

void GraphicsPolygonItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QPointF delta = event->pos() - event->lastPos();
    if (m_left_mouse_btn_pressed) {//鼠标左键按住&&拖动
        OperationType type = CursorTypeToOperation(cursor().shape());
        if (OperationType::POLYGON_MOVE == type) {
            moveBy(delta.x(), delta.y());
        } else if (OperationType::HORI_EDGE_MOVE == type) {
//            m_subitems_manager.SelectedEdgeMove(m_selected_edge_type, delta.y());
            emit SigEdgeMove(m_selected_edge_type, delta.y());
        } else if (OperationType::VERT_EDGE_MOVE == type) {
//            m_subitems_manager.SelectedEdgeMove(m_selected_edge_type, delta.x());
            emit SigEdgeMove(m_selected_edge_type, delta.x());
        }
    }

    //根据Cursor类型，实现边框的
}

void GraphicsPolygonItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    QPointF pt = event->pos();
    if (Qt::LeftButton == event->button()) {
        m_left_mouse_btn_pressed = true;
        if (LeftEdgePath().contains(pt)) {
            m_selected_edge_type = SelectedEdgeType::EDGE_LEFT;
        } else if (RightEdgePath().contains(pt)) {
            m_selected_edge_type = SelectedEdgeType::EDGE_RIGHT;
        } else if (UpEdgePath().contains(pt)) {
            m_selected_edge_type = SelectedEdgeType::EDGE_UP;
        } else if (DownEdgePath().contains(pt)) {
            m_selected_edge_type = SelectedEdgeType::EDGE_DOWN;
        }
    }
}

void GraphicsPolygonItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    Q_UNUSED(event);
    if (Qt::LeftButton == event->button()) {
        m_left_mouse_btn_pressed = false;
        m_subitems_manager.UpdateBoundingRect();
    }

    m_selected_edge_type = SelectedEdgeType::NONE;
}

GraphicsPolygonItem::OperationType GraphicsPolygonItem::CursorTypeToOperation(Qt::CursorShape cursor_shape) const {
    switch (cursor_shape) {
    case Qt::SizeAllCursor:
        return OperationType::POLYGON_MOVE;
        break;
    case Qt::SizeHorCursor:
        return OperationType::VERT_EDGE_MOVE;
        break;
    case Qt::SizeVerCursor:
        return OperationType::HORI_EDGE_MOVE;
        break;
    default:
        return OperationType::NONE;
        break;
    }
}

QPainterPath GraphicsPolygonItem::UpEdgePath() const {
    QPainterPath up_path;
    up_path.moveTo(m_subitems_manager.GetOuterRectF().topLeft() + QPointF(m_corner_width, 0));
    up_path.lineTo(m_subitems_manager.GetOuterRectF().topRight()  - QPointF(m_corner_width, 0));
    return m_path_stroker.createStroke(up_path);
}

QPainterPath GraphicsPolygonItem::DownEdgePath() const {
    QPainterPath down_path;
    down_path.moveTo(m_subitems_manager.GetOuterRectF().bottomLeft() + QPointF(m_corner_width, 0));
    down_path.lineTo(m_subitems_manager.GetOuterRectF().bottomRight() - QPointF(m_corner_width, 0));
    return m_path_stroker.createStroke(down_path);
}

QPainterPath GraphicsPolygonItem::RightEdgePath() const {
    QPainterPath right_path;
    right_path.moveTo(m_subitems_manager.GetOuterRectF().topRight() + QPointF(0, m_corner_width));
    right_path.lineTo(m_subitems_manager.GetOuterRectF().bottomRight() - QPointF(0, m_corner_width));
    return m_path_stroker.createStroke(right_path);;
}

QPainterPath GraphicsPolygonItem::LeftEdgePath() const {
    QPainterPath left_path;
    left_path.moveTo(m_subitems_manager.GetOuterRectF().topLeft() + QPointF(0, m_corner_width));
    left_path.lineTo(m_subitems_manager.GetOuterRectF().bottomLeft() - QPointF(0, m_corner_width));
    return m_path_stroker.createStroke(left_path);;
}

bool GraphicsPolygonItem::OnHorizontalEdge(const QPointF &pt) const {
    if (UpEdgePath().contains(pt) || DownEdgePath().contains(pt)) {
        return true;
    } else {
        return false;
    }
}

bool GraphicsPolygonItem::OnVerticalEdge(const QPointF &pt) const {
    if (LeftEdgePath().contains(pt) || RightEdgePath().contains(pt)) {
        return true;
    } else {
        return false;
    }
}
